const app = require('./src/app');
const database = require('./src/config/database');
const config = require('./src/config/appConfiguration');

database().then( info => {
    console.log(`Connect to ${info.host}:${info.port}/${info.name}`);
    app.listen(config.PORT, function () {
        console.log(`Example app listening on port ${config.PORT}!`);
    });
}).catch(() => {
    console.log('Unable to connect to database');
    process.exit(1);
});
