var express = require('express');
var path = require('path');
var router = express.Router();
const multer = require('multer');

const storage = multer.diskStorage({
    destination: './src/public/images',
    filename: function(req, file, cb){
        cb(null,file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

const upload  = multer({
    storage: storage,
    limits: { fileSize: 2 * 1024 * 1024},
    fileFilter: function (req, file, cb) {
        checkFileType(file, cb);
    }
}).single('photo');

function checkFileType(file, cb){
    // Allowed ext
    const filetypes = /jpeg|jpg|png|gif/;
    // Check ext
    const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    // Check mime
    const mimetype = filetypes.test(file.mimetype);

    if(mimetype && extname){
        return cb(null,true);
    } else {
        cb('Error: Images Only!');
    }
}

router.post('/create-fucking-file', (req, res) => {

    upload(req, res, (err) => {
        if (err) {
            res.render('index', {
                msg: err
            });
        } else {
            if (req.file === undefined) {

            }  else {
                console.log(req.file);
                res.render('index2', { file: `./../images/${req.file.filename}`})
            }
        }
    })
});

module.exports = router;