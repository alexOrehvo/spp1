const express = require('express');
const bodyParser = require('body-parser');
const path =require('path');
const upload = require('./routing/upload-routing');
const app = express();

const task = require('./domain/task');

app.set('view engine', 'ejs');
app.set('views', './src/views');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/todo-list',upload);

var showCompletedTask = false;
var sortBy = "title";

app.get('/', function (req, res) {
    showCompletedTask = false;
    res.render('index');
});

app.get('/todo-list', function (req, res) {
    task.find({}).then( tasks => {
        res.render('workspace/workspace-layout', {tasks: tasks, showCompletedTask: showCompletedTask});
    });
});

app.post('/todo-list/create', (req, res) => {
    const { title, done, deadline } = req.body;

    task.create({
        title: title,
        done: done,
        deadline: deadline
    });
    res.redirect('/todo-list');
});

app.get('/todo-list/search', (req, res) => {
    const title = req.query.title;

    task.find({"title": title}).then(tasks => {
        res.render('workspace/workspace-layout', {tasks: tasks, showCompletedTask: showCompletedTask});
    });
});

app.get('/todo-list/filter', (req, res) => {
    console.log(3);
    showCompletedTask = req.query.done;
    if(req.query.sort) sortBy = req.query.sort;

    if (!sortBy.localeCompare('name')) {
        task.find({}).sort({"title": 1}).then(tasks =>
            res.render('workspace/workspace-layout', {tasks: tasks, showCompletedTask: showCompletedTask}));
    } else {
        if (!sortBy.localeCompare('date')) {
            task.find({}).sort({"deadline": 1}).then(tasks =>
                res.render('workspace/workspace-layout', {tasks: tasks, showCompletedTask: showCompletedTask}));
        } else {
            task.find({}).sort({"done": 1}).then(tasks =>
                res.render('workspace/workspace-layout', {tasks: tasks, showCompletedTask: showCompletedTask}));
        }
    }
});

app.post('/todo-list/done', (req, res) => {
    const status = req.body;
    if (!Array.isArray(status.done))
        status.done = new Array(status.done);


    status.done.forEach(id => {
        task.updateOne({_id: id}, {done: true}).then();
    });
    res.redirect("/todo-list");
});

///////////////////////////////////////////////


/*app.post('/create-fucking-file', multer(multerConf).single('photo'), (req, res) => {
    uplo
    res.redirect('/', {file: `images/${req.file.filename}`});
});*/

module.exports = app;