var hiddenForm = true;

function showForm(id) {
    var element = document.getElementById(id);
    if (hiddenForm) {
        element.className -= ' hidden';
    } else {
        element.className += ' hidden';
    }
    hiddenForm = !hiddenForm;
}

function hideForm(id) {
    var element = document.getElementById(id);
    element.className += ' hidden';
    hiddenForm = true;
}