const inputs = document.getElementsByName('photo');

for (let i=0; i < inputs.length; i++) {
    inputs[i].addEventListener('change', function (ev) {
        const file = ev.target.files[0];

        console.log(file);

        const formData = new FormData();
        formData.append('', file);
        formData.append('id', ev.id);

        const req = new XMLHttpRequest();
        req.open('POST', '/todo-list/add-file');
        req.send(formData);
    });
}

